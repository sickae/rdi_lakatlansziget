﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LakatlanSziget
{
    class Program
    {
        static Random rand = new Random();
        static int maxHeight = 10;
        static int elements = 20;
        static void Main(string[] args)
        {
            int[] data = new int[elements];
            FillArray(ref data);
            ShowOnConsole(data);
            int[] peak = HighestPeakData(data);
            Console.WriteLine($"HighestPeakData: {peak[0]}, {peak[1]}");
            Console.WriteLine($"HighestOccurences: {HighestOccurences(data)}");
            int[] interval = LongestInterval(data);
            Console.WriteLine($"LongestInterval: {interval[0]}, {interval[1]}");
            Console.WriteLine(IsHighestOnLongest(data));
        }

        // Valósítsuk meg a tömb véletlenszerű feltöltését!40% a valószínűsége, hogy egy helyen szigetet találunk. A sziget aktuális magassága 1 és 10 közötti véletlen szám.
        static void FillArray(ref int[] data)
        {
            for (int i = 0; i < data.Length; i++)
            {
                if (rand.NextDouble() < 0.4) data[i] = rand.Next(maxHeight + 1);
            }
        }

        // Jelenítsük meg a mérési eredményeket a képernyőn!
        static void ShowOnConsole(int[] data)
        {
            for (int i = maxHeight; i > 0; i--)
            {
                for (int j = 0; j < data.Length; j++) Console.Write(data[j] >= i ? "*" : " ");
                Console.WriteLine();
            }
        }

        // Határozzuk meg, hogy hol található (először) a legmagasabb pont, és mennyi ennek a magassága!
        static int[] HighestPeakData(int[] data)
        {
            for(int i = maxHeight; i > 0; i--)
            {
                for(int j = 0; j < data.Length; j++)
                {
                    if (data[j] == i) return new int[] { j, i };
                }
            }
            return new int[] { 0, 0 };
        }

        // Adjuk meg, hogy a legmagasabb pont hányszor fordult elő a repülés során!
        static int HighestOccurences(int[] data)
        {
            int highest = HighestPeakData(data).Last();
            int occurences = 0;
            for(int i = 0; i < data.Length; i++)
            {
                if (data[i] == highest) occurences++;
            }
            return occurences;
        }

        // Határozzuk meg a leghosszabb szigetszakasz kezdő- és végpontját!
        static int[] LongestInterval(int[] data)
        {
            Dictionary<int, int> intervals = new Dictionary<int, int>();
            int current_idx = -1;
            for(int i = 0; i < data.Length; i++)
            {
                if (data[i] > 0)
                {
                    if (current_idx >= 0) intervals[current_idx]++;
                    else
                    {
                        current_idx = i;
                        intervals.Add(current_idx, 1);
                    }
                }
                else current_idx = -1;
            }
            if (intervals.Count == 0) return new int[] { -1, -1 };
            int maxKey = intervals.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;
            return new int[] { maxKey, maxKey + intervals[maxKey] };
        }

        // Adjuk meg, hogy a leghosszabb szigeten található-e az első maximális magasságú mérési pont!
        static bool IsHighestOnLongest(int[] data)
        {
            int[] highest = HighestPeakData(data);
            int[] longest = LongestInterval(data);
            return highest.First() >= longest.First() && highest.First() <= longest.Last();
        }
    }
}
